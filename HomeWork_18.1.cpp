﻿// HomeWork_18.1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

template<typename T> 
class Stack
{
  private:
    T* mas_temp;
    T* mas;
    int top;
    int size;
  public:
      Stack(int _size) :size(_size) { mas = (T*)malloc(size * sizeof(T));  top = 0; }
    int StackSize();
    void Push(T item);  //Помещаем элемент в стек
    T pop();    //Выталкиваем элемент из стека
    T AddElem(T item);
};
template <typename T>
int Stack<T>::StackSize()
{
    return size;
}
//Помещаем элемент в стек
template <typename T>
void Stack<T>::Push(T item)
{
    if (top >= size)
    {
        cout << "\nStack is full. \n";
        return;
    }
    mas[top] = item;
    top++;
}

//Выталкиваем элемент из стека
template <typename T>
T Stack<T>::pop()
{
    if (top <= 0)
    {
        cout << "\nStack is empty.\n";
        return 0;
    }
    top--;
    return mas[top];
}

//Добавляем елемент в стек
template <typename T>
T Stack<T>::AddElem(T item)
{
    size++;
    T* new_mas = new T[size];
    for (int i = 0;i < size - 1;++i)
    
        new_mas[i] = mas[i];
    
        delete[] mas;
        mas = new_mas;
        mas[top] = item;
        top++; 
        return 0;
}

int main()
{
    int i, size;
    Stack<int> st1(7);
    for (i = 0; i < 7; i++)
    {
        st1.Push(i + 7);
    }
    st1.AddElem(19);
    size = st1.StackSize();
    cout << size << endl;
    for (i = 0; i < size - 1; i++)
    cout << st1.pop() << ' ';
    cout << "\n" << endl;
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
